# CFP

To clone project, run:
```bash
git clone --recursive git@gitlab.com:asmirnov1005/cfp.git
```

To create the environment:
```bash
conda env create -f environment.yml
```

To activate the new environment:
```bash
conda activate cfp
```

To update the environment (if environment.yml has been changed):
```bash
conda env update --file environment.yml --prune
```
