var calculatorModulePrimes = new (function CalculatorModulePrimes() {
  
  var self;
  
  var decompositions = [{ 0 : 1 }, { 1 : 1 }];
  
  function isDecomposed(n) {
    return n < decompositions.length;
  }
  
  function decompositionDegree(decomposition) {
    var d = 0;
    
    for (p in decomposition)
      d += decomposition[p];
    
    return d;
  }
  
  this.decompose = function (n = 0) {
    var decomposition = {};
    
    if (n < 0) return false;
    
    if (isDecomposed(n))
      return decompositions[n];
    
    for (var k = decompositions.length; k < n; k++)
      self.decompose(k);
    
    for (var d = 2; d <= Math.sqrt(n); d++) {
      if (!self.isPrime(d)) continue;
      if (n % d != 0) continue;
      
      var m = n / d;
      
      for (var p in decompositions[m])
        decomposition[p] = decompositions[m][p];
      decomposition[d] = decomposition[d] ? decomposition[d] + 1 : 1;
      
      break;
    }
    if (Object.keys(decomposition).length == 0)
      decomposition[n] = 1;
    
    decompositions.push(decomposition);
    
    return decomposition;
  }
  
  this.isPrime = function (p) {
    if (p < 2) return false;
    
    if (!isDecomposed(p))
      self.decompose(p);
    
    return decompositionDegree(decompositions[p]) == 1;
  }
  
  this.firstNPrimes = function (n) {
    var primes = [];
    
    if (n < 1) return primes;
    
    var m = 2;
    while (primes.length < n) {
      if (self.isPrime(m)) primes.push(m);
      m++;
    }
    
    return primes;
  }
  
  this.primesUpTo = function (n) {
    var primes = [];
    
    for (var p = 2; p <= n; p++)
      if (self.isPrime(p)) primes.push(p);
    
    return primes;
  }
  
  this.pi = function (x) {
    return self.primesUpTo(x).length;
  }
  
  this.mu = function (n) {
    var decomposition = self.decompose(n);
    var multiplicities = Object.keys(decomposition).map(function (p) { return decomposition[p] });
    
    if (n == 1)
      return 1;
    
    if (multiplicities.reduce((n, m) => n * m) > 1)
      return 0;
    
    return Math.pow(-1, multiplicities.length % 2);
  }
  
  this.gpf = function (n) {
    if (n < 2) return null;
    
    return Math.max(...Object.keys(self.decompose(n)));
  }
  
  this.nu = function (n) {
    return self.pi(self.gpf(n));
  }
  
  this.ord = function (p, n) {
    return self.decompose(n)[p] || 0;
  }
  
  this.init = function () {
    self = this;
  }
  
  this.init();
  
})();