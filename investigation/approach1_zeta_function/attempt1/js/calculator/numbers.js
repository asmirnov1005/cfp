var calculatorModuleNumbers = new (function CalculatorModuleNumbers() {
  
  var self;
  
  this.factorial = function (n) {
    var result = 1;
    
    for (var i = 1; i <= n; i++) {
      result *= i;
    }
    
    return result;
  }
  
  this.C = function (n, k) {
    return Math.round(self.factorial(n) / (self.factorial(k) * self.factorial(n - k)));
  }
  
  this.formatBigInt = function (n) {
    return String(n).split('')
                    .reverse()
                    .join('')
                    .match(/.{1,3}/g)
                    .join(' ')
                    .split('')
                    .reverse()
                    .join('');
  }
  
  this.formatSmallDouble = function (x, n = 15) {
    if (typeof(x) == "string")
      x = parseFloat(x);
    if (x <= 0 || x >= 1)
      return String(x);
    
    return '0.' + self.formatBigInt(Math.round(x * Math.pow(10, n)));
  }
  
  this.init = function () {
    self = this;
  }
  
  this.init();
  
})();