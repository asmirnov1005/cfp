var calculator = new (function Calculator() {
  
  this.numbers = calculatorModuleNumbers;
  this.primes = calculatorModulePrimes;
  this.continuedFractionFor = calculatorConstructorContinuedFraction;
  
})();