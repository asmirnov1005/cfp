var calculatorConstructorContinuedFraction = function (sequence) {
  return new (function CalculatorConstructorContinuedFraction(s) {
    
    var self;
    
    var numerators = [];
    var denominators = [];
    
    function fractionFor(convergent) {
      return convergent.numerator / convergent.denominator;
    }
    
    this.getConvergent = function (i) {
      if (i < 0 || i > s.length - 1) return null;
      
      if (i > numerators.length)
        self.getConvergent(i - 1);
      if (i == numerators.length) {
        numerators.push(getNumerator(i));
        denominators.push(getDenominator(i));
      }
      
      return {
        numerator : numerators[i],
        denominator : denominators[i]
      };
      
      function getNumerator(j) {
        if (j == 0)
          return s[0];
        if (j == 1)
          return s[1] * numerators[0] + 1;
        
        return s[j] * numerators[j - 1] + numerators[j - 2];
      }
      
      function getDenominator(j) {
        if (j == 0)
          return 1;
        if (j == 1)
          return s[1] * denominators[0];
        
        return s[j] * denominators[j - 1] + denominators[j - 2];
      }
    }
    
    this.getConvergentFraction = function (i) {
      return fractionFor(self.getConvergent(i));
    }
    
    this.getBestConvergent = function () {
      return self.getConvergent(s.length - 1);
    }
    
    this.getBestConvergentFraction = function () {
      return fractionFor(self.getBestConvergent());
    }
    
    this.init = function () {
      self = this;
    }
    
    this.init();
    
  })(sequence);
}