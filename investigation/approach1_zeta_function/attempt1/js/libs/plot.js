(function() {
  
  /*
    
    �������� �����
    
  */
  
  function Plot(canvasDomElement, options = {}) {
    
    // ��������� �� ���������
    
    const defaultOptions = {
      canvas        : {},
      grid          : false,
      timer         : false,
      subscriptions : {},
    };
    
    // ������������� ��������
    
    init();
    
    // ��������� ���������� � ���������
    
    const plane = new Plane();
    const canvas = new Canvas(canvasDomElement, plane, options.canvas);
    
    let self;
    
    // ��������� ������
    
    function init() {
      initOptions();
    }
    
    function initOptions() {
      options = mergeDictionaries(defaultOptions, options);
      options.canvas.subscriptions = options.subscriptions;
      options.canvas.grid = options.grid;
    }
    
    // ��������� ����������
    
    // ��������� ������
    
    this.addCurve = plane.addCurve;
    
    this.addImage = plane.addImage;
    
    this.addLine = plane.addLine;
    
    this.addPoint = plane.addPoint;
    
    this.addPolyline = plane.addPolyline;
    
    this.addSegment = plane.addSegment;
    
    this.clean = canvas.clean;
    
    this.draw = canvas.draw;
    
    this.init = function () {
      self = this;
      
      self.initTimer();
    }
    
    this.initTimer = function () {
      if (options.timer == false)
        return;
      if (options.timer == true)
        options.timer = {};
      self.timer = new Timer(canvas, options.timer);
    }
    
    this.removeAll = plane.removeAll;
    
    // ������������� �������
    
    this.init();
    
  }
  
  /*
    
    �� �������������� ����� ������
    
  */
  
  /*
    ������
  */
  
  function Canvas(domElement, plane, options = {}) {
    
    // ��������� �� ���������
    
    const defaultSize = {
      width   : 400,
      height  : 300
    };
    const centerPosition = {
      x : Math.floor((options.width || defaultSize.width) / 2),
      y : Math.floor((options.height || defaultSize.height) / 2),
    };
    const defaultOptions = {
      width     : defaultSize.width,
      height    : defaultSize.height,
      scale     : 20,
      origin    : {
        x : centerPosition.x,
        y : centerPosition.y,
      },
      color     : '#FFFFFF',
      zoomSpeed : 1.1,
      movable   : true,
      zoomable  : true,
      grid      : false,
      subscriptions : {
        mouseCoordinates : function (x, y) {},
        mouseClick : function (x, y) {},
        planeVisiblePart : function (borders) {},
      },
    };
    
    // ������������� ��������� ����������
    
    init();
    
    // ��������� ���������� � ���������
    
    let self;
    
    // ��������� ������
    
    function getMousePosition(event) {
      const bcRect = domElement.getBoundingClientRect();
      
      return {
        x : event.clientX - bcRect.left,
        y : event.clientY - bcRect.top,
      };
    }
    
    function init() {
      initOptions();
      initDomElement();
      initEvents();
    }
    
    function initDomElement() {
      domElement.setAttribute('width', options.width);
      domElement.setAttribute('height', options.height);
      
      initDomElementStyle();
    }
    
    function initDomElementStyle() {
      domElement.style.cursor = 'pointer';
    }
    
    function initDragAndDropEvents() {
      let isDragged = false;
      let previousPosition = {};
      
      domElement.onmousedown = onMouseDown;
      document.onmouseup = onMouseUp;
      document.onmousemove = onMouseMove;
      
      function onMouseDown(event) {
        isDragged = true;
        updatePreviousPosition(event);
      }
      
      function onMouseUp(event) {
        shift(event);
        isDragged = false;
      }
      
      function onMouseMove(event) {
        shift(event);
      }
      
      function shift(event) {
        if (!isDragged || !options.movable)
          return;
        
        options.origin.x += event.clientX - previousPosition.x;
        options.origin.y += event.clientY - previousPosition.y;
        updatePreviousPosition(event);
        self.draw();
      }
      
      function updatePreviousPosition(event) {
        previousPosition = {
          x : event.clientX,
          y : event.clientY
        };
      }
    }
    
    function initEvents() {
      initDragAndDropEvents();
      initZoomEvent();
      initSubscriptionEvents();
    }
    
    function initOptions() {
      options = mergeDictionaries(defaultOptions, options);
    }
    
    function initSubscriptionEvents() {
      domElement.onmouseover = onMouseOverSubscriptions;
      domElement.onmousemove = onMouseMoveSubscriptions;
      domElement.onmouseleave = onMouseLeaveSubscriptions;
      domElement.onclick = onMouseClickSubscriptions;
      
      function onMouseOverSubscriptions(event) {}
      
      function onMouseMoveSubscriptions(event) {
        callWithCurrentMousePosition(options.subscriptions.mouseCoordinates, event);
      }
      
      function onMouseLeaveSubscriptions(event) {
        options.subscriptions.mouseCoordinates(undefined, undefined);
      }
      
      function onMouseClickSubscriptions(event) {
        callWithCurrentMousePosition(options.subscriptions.mouseClick, event);
      }
      
      function callWithCurrentMousePosition(subscriptionFunction, event) {
        const mousePosition = getMousePosition(event);
        
        subscriptionFunction(
          self.restoreX(mousePosition.x),
          self.restoreY(mousePosition.y)
        );
      }
    }
    
    function initZoomEvent() {
      domElement.onwheel = function (event) {
        const mousePosition = getMousePosition(event);
        const zoomCoefficient = Math.pow(options.zoomSpeed, -event.deltaY / Math.abs(event.deltaY));
        
        if (!options.zoomable || !options.movable)
          return;
        event.preventDefault();
        options.scale *= zoomCoefficient;
        options.origin.x = mousePosition.x + zoomCoefficient * (options.origin.x - mousePosition.x);
        options.origin.y = mousePosition.y + zoomCoefficient * (options.origin.y - mousePosition.y);
        self.draw();
      }
    }
    
    function onDrawSubscriptions() {
      options.subscriptions.planeVisiblePart(self.getBorders());
    }
    
    // ��������� ������
    
    this.clean = function () {
      const context = domElement.getContext('2d');
      
      context.fillStyle = options.color;
      context.fillRect(0, 0, options.width, options.height);
    }
    
    this.convertX = function (x) {
      return options.origin.x + Math.floor(options.scale * x);
    }
    
    this.convertY = function (y) {
      return options.origin.y - Math.floor(options.scale * y);
    }
    
    this.draw = function () {
      const planeCurves = plane.objects.filter((object) => !(object instanceof PlanePoint));
      const planePoints = plane.objects.filter((object) => object instanceof PlanePoint);
      
      onDrawSubscriptions();
      self.clean();
      if (self.grid)
        self.grid.draw();
      for (let i in planeCurves)
        planeCurves[i].draw(self);
      for (let i in planePoints)
        planePoints[i].draw(self);
    }
    
    this.getBorders = function () {
      return {
        left    : -options.origin.x / options.scale,
        right   : (options.width - options.origin.x) / options.scale,
        bottom  : (options.origin.y - options.height) / options.scale,
        top     : options.origin.y / options.scale,
      };
    }
    
    this.getContext = function () {
      return domElement.getContext('2d');
    }
    
    this.getOrigin = function () {
      return options.origin;
    }
    
    this.getPlane = function () {
      return plane;
    }
    
    this.getScale = function () {
      return options.scale;
    }
    
    this.init = function () {
      self = this;
      
      self.grid = options.grid ? new Grid(self, options.grid) : undefined;
    }
    
    this.restoreX = function (x) {
      return -(options.origin.x - x) / options.scale;
    }
    
    this.restoreY = function (y) {
      return (options.origin.y - y) / options.scale;
    }
    
    // ������������� �������
    
    this.init();
    
  }
  
  function Grid(canvas, options = {}) {
    
    // ��������� �� ���������
    
    const defaultOptions = {
      density   : 1,
      color     : '#EEEEEE',
    };
    
    // ������������� ��������� ����������
    
    init();
    
    // ��������� ���������� � ���������
    
    let self;
    
    // ��������� ������
    
    function drawVerticalAxes() {
      const borders = canvas.getBorders();
      const plane = canvas.getPlane();
      const dx = options.density;
      
      let x = Math.floor(borders.left / dx) * dx;
      while (x <= Math.floor(borders.right / dx + 1) * dx) {
        const axisColor = Math.abs(x) < dx ? options.axesColor : options.color;
        
        let verticalAxis = new PlaneLine(plane, [x, -1], [x, 1], { color : axisColor });
        
        verticalAxis.draw(canvas);
        x += dx;
      }
    }
    
    function drawHorizontalAxes() {
      const borders = canvas.getBorders();
      const plane = canvas.getPlane();
      const dy = options.density;
      
      let y = Math.floor(borders.bottom / dy) * dy;
      while (y <= Math.floor(borders.top / dy + 1) * dy) {
        const axisColor = Math.abs(y) < dy ? options.axesColor : options.color;
        
        let horizontalAxis = new PlaneLine(plane, [-1, y], [1, y], { color : axisColor });
        
        horizontalAxis.draw(canvas);
        y += dy;
      }
    }
    
    function init() {
      initOptions();
    }
    
    function initOptions() {
      options = mergeDictionaries(defaultOptions, options);
      
      if (options.color && !options.axesColor)
        options.axesColor = getDarkerColor(options.color);
      
      function getDarkerColor(color) {
        const colorDiff = '#222222';
        
        return '#' + Math.max(colorToNumber(color) - colorToNumber(colorDiff), 0)
                         .toString(16)
                         .toUpperCase();
      }
      
      function colorToNumber(color) {
        return eval(color.replace('#', '0x'));
      }
    }
    
    // ��������� ������
    
    this.draw = function () {
      drawVerticalAxes();
      drawHorizontalAxes();
    }
    
    this.init = function () {
      self = this;
    }
    
    // ������������� �������
    
    this.init();
    
  }
  
  function Timer(canvas, options = {}) {
    
    // ��������� �� ���������
    
    const defaultOptions = {
      beginning : 0,
      fps       : 1,
    };
    
    // ��������� ���������� � ���������
    
    let self;
    
    let delay;
    let onPause;
    let tick;
    
    // ������������� ��������� ����������
    
    init();
    
    // ��������� ������
    
    function init() {
      initOptions();
      initVariables();
    }
    
    function initOptions() {
      options = mergeDictionaries(defaultOptions, options);
    }
    
    function initVariables() {
      delay = 1000 / options.fps;
      onPause = true;
      tick = options.beginning;
    }
    
    function makeTickActions() {
      const planeObjects = canvas.getPlane().objects;
      
      self.subscriptionFunction(tick);
      for (let i in planeObjects) {
        if (!planeObjects[i].onTickSubscriptionFunction)
          continue;
        
        planeObjects[i].onTickSubscriptionFunction(tick);
      }
      canvas.draw();
    }
    
    function makeTick() {
      if (onPause)
        return;
      makeTickActions();
      setTimeout(() => {
        tick++;
        if (tick == Number.MAX_SAFE_INTEGER)
          tick = 0;
        makeTick();
      }, delay);
    }
    
    // ��������� ����������
    
    // ��������� ������
    
    this.init = function () {
      self = this;
    }
    
    this.initSubscriptionFunction = function () {
      self.subscriptionFunction = () => {};
    }
    
    this.pause = function () {
      onPause = true;
    }
    
    this.play = function (subscriptionFunction) {
      onPause = false;
      if (subscriptionFunction && typeof(subscriptionFunction) == "function")
        self.subscriptionFunction = subscriptionFunction;
      makeTick();
    }
    
    this.start = function (subscriptionFunction) {
      initVariables();
      self.play(subscriptionFunction);
    }
    
    this.stop = function () {
      this.pause();
      initVariables();
    }
    
    // ������������� �������
    
    this.init();
    
  }
  
  function Plane() {
    
    // ��������� ���������� � ���������
    
    let self;
    
    // ��������� ������
    
    function addObject(Class, ...args) {
      let object = new Class(self, ...args);
      
      object.index = self.objects.length == 0 ? 0 : self.objects[self.objects.length - 1].index + 1;
      self.objects.push(object);
      
      return object;
    }
    
    // ��������� ����������
    
    this.objects = [];
    
    // ��������� ������
    
    this.addCurve = function (...args) {
      return addObject(PlaneCurve, ...args);
    }
    
    this.addImage = function (...args) {
      return addObject(PlotImage, ...args)
    }
    
    this.addLine = function (...args) {
      return addObject(PlaneLine, ...args);
    }
    
    this.addPoint = function (...args) {
      return addObject(PlanePoint, ...args);
    }
    
    this.addPolyline = function (...args) {
      return addObject(PlanePolyline, ...args);
    }
    
    this.addSegment = function (...args) {
      return addObject(PlaneSegment, ...args);
    }
    
    this.init = function () {
      self = this;
    }
    
    this.remove = function (objectToRemove) {
      const objectListIndex = self.objects.map((object) => object.index).indexOf(objectToRemove.index);
      
      if (objectListIndex != -1)
        self.objects.splice(objectListIndex, 1);
    }
    
    this.removeAll = function () {
      self.objects = [];
    }
    
    // ������������� �������
    
    this.init();
    
  }
  
  function PlaneObject(plane) {
    
    let self;
    
    let subscriptions;
    
    // ��������� ������
    
    this.draw = function (canvas) {
      if (self.style.visible === false)
        return;
    }
    
    this.hide = function () {
      self.style.visible = false;
    }
    
    this.init = function () {
      self = this;
      
      self.style = {
        visible : false,
      };
    }
    
    this.onTick = function (subscriptionFunction) {
      if (subscriptionFunction && typeof(subscriptionFunction) == "function") {
        self.onTickSubscriptionFunction = subscriptionFunction;
      }
    }
    
    this.show = function () {
      self.style.visible = true;
    }
    
    this.stop = function () {
      self.onTickSubscriptionFunction = undefined;
    }
    
    this.remove = function () {
      plane.remove(self);
    }
    
    // ������������� �������
    
    this.init();
    
  }
  
  function PlanePoint(plane, x, y, style = {}) {
    
    // ������������
    
    PlaneObject.apply(this, arguments);
    
    // ��������� �� ���������
    
    const defaultStyle = {
      visible     : true,
      size        : 1,
      color       : '#000000',
      borderColor : '#000000',
    };
    
    // ��������� ���������� � ���������
    
    let self;
    
    // ��������� ������
    
    function initCoordinates() {
      self.x = typeof x == "number" ? x : 0;
      self.y = typeof y == "number" ? y : 0;
    }
    
    function initStyle() {
      if (style.color && !style.borderColor)
        style.borderColor = style.color;
      self.style = mergeDictionaries(defaultStyle, style);
    }
    
    // ��������� ������
    
    this.draw = function (canvas) {
      let context = canvas.getContext();
      
      if (self.style.visible === false)
        return;
      
      context.beginPath();
      context.arc(
        canvas.convertX(self.x),
        canvas.convertY(self.y),
        self.style.size / 2,
        0, 2 * Math.PI
      );
      context.fillStyle = self.style.color;
      context.fill();
      context.strokeStyle = self.style.borderColor;
      context.stroke();
    }
    
    this.init = function () {
      self = this;
      
      initCoordinates();
      initStyle();
    }
    
    // ������������� �������
    
    this.init();
    
  }
  
  function PlaneSegment(plane, start, end, style = {}) {
    
    // ������������
    
    PlaneObject.apply(this, arguments);
    
    // ��������� �� ���������
    
    const defaultStyle = {
      visible     : true,
      color       : '#000000',
      lineWidth   : 1,
      vertices    : {
        rewrite : false,
      },
    };
    
    // ��������� ���������� � ���������
    
    let self;
    
    // ��������� ������
    
    function drawSegment(canvas) {
      let context = canvas.getContext();
      
      context.beginPath();
      context.moveTo(canvas.convertX(self.start.x), canvas.convertY(self.start.y));
      context.lineTo(canvas.convertX(self.end.x), canvas.convertY(self.end.y));
      context.strokeStyle = self.style.color;
      context.lineWidth = self.style.lineWidth;
      context.stroke();
    }
    
    function drawVertices(canvas) {
      if (self.style.vertices.visible === false)
        return;
      
      self.start.draw(canvas);
      self.end.draw(canvas);
    }
    
    function initPoint(pObject) {
      if (isObject(pObject)) {
        if (pObject instanceof PlanePoint && !self.style.vertices.rewrite) {
          return pObject;
        } else {
          if (pObject instanceof PlanePoint)
            pObject.remove();
          return new PlanePoint(plane, pObject.x, pObject.y, self.style.vertices);
        }
      } else if (Array.isArray(pObject)) {
        return new PlanePoint(plane, pObject[0], pObject[1], self.style.vertices);
      } else {
        return undefined;
      }
    }
    
    function initStyle() {
      if (!style.vertices || (isObject(style.vertices) && Object.keys(style.vertices).length == 0))
        style.vertices = { visible : false };
      
      self.style = mergeDictionaries(defaultStyle, style);
    }
    
    function initVertices() {
      self.start = initPoint(start);
      self.end = initPoint(end);
    }
    
    // ��������� ������
    
    this.draw = function (canvas) {
      if (self.style.visible === false)
        return;
      
      drawSegment(canvas);
      drawVertices(canvas);
    }
    
    this.init = function () {
      self = this;
      
      initStyle();
      initVertices();
    }
    
    // ������������� �������
    
    this.init();
  }
  
  function PlanePolyline(plane, ...args) {
    
    // ������������
    
    PlaneObject.apply(this, arguments);
    
    // ��������� �� ���������
    
    const defaultStyle = {
      visible     : true,
      color       : '#000000',
      lineWidth   : 1,
      vertices    : {
        rewrite : false,
      },
    };
    
    // ��������� ���������� � ���������
    
    let self;
    
    // ��������� ������
    
    function initSegments() {
      self.segments = [];
      for (let i = 0; i < args.length - 1; i++) {
        let segment = new PlaneSegment(plane, args[i], args[i + 1], self.style);
        
        if (i < args.length - 2)
          segment.end.style.visible = false;
        self.segments.push(segment);
      }
    }
    
    function initStyle() {
      let style = args.slice(-1)[0];
      
      if (isObject(style) &&
          Object.keys(style).length > 0 &&
          !!~Object.keys(defaultStyle).indexOf(Object.keys(style)[0])) {
        if (!style.vertices ||
            (isObject(style.vertices) && Object.keys(style.vertices).length == 0))
          style.vertices = { visible : false };
        self.style = mergeDictionaries(defaultStyle, style);
        args.pop();
      } else {
        self.style = defaultStyle;
      }
    }
    
    // ��������� ������
    
    this.draw = function(canvas) {
      for (let i in self.segments)
        self.segments[i].draw(canvas);
    }
    
    this.init = function () {
      self = this;
      
      initStyle();
      initSegments();
    }
    
    // ������������� �������
    
    this.init();
  }
  
  function PlaneLine(plane, P, Q, style = {}) {
    
    // ������������
    
    PlaneObject.apply(this, arguments);
    
    // ��������� �� ���������
    
    const defaultStyle = {
      visible     : true,
      color       : '#000000',
      lineWidth   : 1,
    };
    
    // ��������� ���������� � ���������
    
    let self;
    
    // ��������� ������
    
    function initPoint(pObject) {
      if (isObject(pObject)) {
        return new PlanePoint(plane, pObject.x, pObject.y, { visible : false });
      } else if (Array.isArray(pObject)) {
        return new PlanePoint(plane, pObject[0], pObject[1], { visible : false });
      } else {
        return undefined;
      }
    }
    
    function initStyle() {
      self.style = mergeDictionaries(defaultStyle, style);
    }
    
    function initPoints() {
      self.P = initPoint(P);
      self.Q = initPoint(Q);
    }
    
    // ��������� ������
    
    this.draw = function (canvas) {
      let context = canvas.getContext();
      let ends = getEnds();
      
      if (self.style.visible === false)
        return;
      
      context.beginPath();
      context.moveTo(canvas.convertX(ends.P.x), canvas.convertY(ends.P.y));
      context.lineTo(canvas.convertX(ends.Q.x), canvas.convertY(ends.Q.y));
      context.strokeStyle = self.style.color;
      context.lineWidth = self.style.lineWidth;
      context.stroke();
      
      function getEnds() {
        const borders = canvas.getBorders();
        const over = 10 / canvas.getScale();
        
        if (self.isVertical())
          return getEndsForVertical();
        else
          return getEndsForSloped();
        
        function getEndsForVertical() {
          return {
            P : {
              x : self.P.x,
              y : borders.bottom - over,
            },
            Q : {
              x : self.P.x,
              y : borders.top + over,
            }
          };
        }
        
        function getEndsForSloped() {
          return {
            P : {
              x : borders.left - over,
              y : applyEquationFor(borders.left - over),
            },
            Q : {
              x : borders.right + over,
              y : applyEquationFor(borders.right + over),
            }
          };
          
          function applyEquationFor(x) {
            const a = (self.P.y - self.Q.y) / (self.P.x - self.Q.x);
            const b = self.P.y - a * self.P.x;
            
            return a * x + b;
          }
        }
      }
    }
    
    this.init = function () {
      self = this;
      
      initStyle();
      initPoints();
    }
    
    this.isVertical = function() {
      return self.P.x == self.Q.x;
    }
    
    // ������������� �������
    
    this.init();
  }
  
  function PlaneCurve(plane, equation, style = {}) {
    
    // ������������
    
    PlaneObject.apply(this, arguments);
    
    // ��������� �� ���������
    
    const defaultStyle = {
      visible     : true,
      color       : '#000000',
      lineWidth   : 1,
      density     : 1,
    };
    
    // ��������� ���������� � ���������
    
    let self;
    
    // ��������� ������
    
    function initStyle() {
      self.style = mergeDictionaries(defaultStyle, style);
    }
    
    // ��������� ������
    
    this.draw = function (canvas) {
      const borders = canvas.getBorders();
      const dx = self.style.density;
      
      let context = canvas.getContext();
      let extendedPivotPoints = [];
      let extendedPivotPoint = {};
      
      if (self.style.visible === false)
        return;
      
      for (let x = Math.floor(borders.left / dx) * dx;
               x <= Math.floor(borders.right / dx + 1) * dx;
               x += dx) {
        try {
          extendedPivotPoint = {
            point : new PlanePoint(plane, x, equation(x), { visible : false }),
            type  : 'vi',
          };
          if (extendedPivotPoint.point.y < borders.bottom || extendedPivotPoint.point.y > borders.top)
            extendedPivotPoint.type = 'invi';
        } catch (error) {
          extendedPivotPoint = { type : 'ne' };
        }
        
        extendedPivotPoints.push(extendedPivotPoint);
      }
      
      for (let i = 0; i < extendedPivotPoints.length - 1; i++) {
        const visiblePairs = ['invi-vi', 'vi-invi', 'vi-vi'];
        
        let extP = extendedPivotPoints[i];
        let extQ = extendedPivotPoints[i + 1];
        
        if (!~visiblePairs.indexOf(extP.type + '-' + extQ.type))
          continue;
        
        let segment = new PlaneSegment(plane, extP.point, extQ.point, self.style);
        segment.draw(canvas);
      }
    }
    
    this.init = function () {
      self = this;
      
      initStyle();
    }
    
    // ������������� �������
    
    this.init();
  }
  
  function PlotImage(plane, x, y, src, style = {}) {
    
    // ������������
    
    PlaneObject.apply(this, arguments);
    
    // ��������� �� ���������
    
    const defaultStyle = {
      visible : true,
      width   : undefined,
      height  : undefined,
    };
    
    // ��������� ���������� � ���������
    
    let self;
    
    let imageElement;
    
    // ��������� ������
    
    function draw(canvas) {
      let context = canvas.getContext();
      let width = self.style.width ? self.style.width : imageElement.naturalWidth;
      let height = self.style.height ? self.style.height : imageElement.naturalHeight;
      
      context.drawImage(
        imageElement,
        canvas.convertX(self.x) - Math.floor(width / 2),
        canvas.convertY(self.y) - Math.floor(height / 2),
        width,
        height
      );
    }
    
    function initCoordinates() {
      self.x = typeof x == "number" ? x : 0;
      self.y = typeof y == "number" ? y : 0;
    }
    
    function initImageElement() {
      imageElement = new Image();
      self.setSrc(src);
    }
    
    function initStyle() {
      self.style = mergeDictionaries(defaultStyle, style);
    }
    
    // ��������� ������
    
    this.draw = function (canvas) {
      if (self.style.visible === false)
        return;
      
      if (imageElement.complete)
        draw(canvas);
      else
        imageElement.addEventListener("load", () => draw(canvas), false);
    }
    
    this.init = function () {
      self = this;
      
      initCoordinates();
      initStyle();
      initImageElement();
    }
    
    this.setSrc = function (src) {
      imageElement.src = src;
    }
    
    // ������������� �������
    
    this.init();
    
  }
  
  /*
    �������
  */
  
  function isObject(item) {
    return (item && typeof item === 'object' && !Array.isArray(item));
  }
  
  function mergeDictionaries(target, ...sources) {
    function recursiveMerge(target, ...sources) {
      if (!sources.length) return target;
      const source = sources.shift();
      
      if (isObject(target) && isObject(source)) { 
        for (let key in source) {
          if (isObject(source[key])) {
            if (!target[key])
              Object.assign(target, { [key] : {} });
            else         
              target[key] = Object.assign({}, target[key]);
            recursiveMerge(target[key], source[key]);
          } else {
            Object.assign(target, { [key] : source[key] });
          }
        }
      }
      
      return recursiveMerge(target, ...sources);
    }
    
    return recursiveMerge(target, ...sources);
  }
  
  /*
    
    �������
    
  */
  
  window.Plot = Plot;
  
}());