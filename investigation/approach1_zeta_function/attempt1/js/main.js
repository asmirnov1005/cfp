﻿$(document).ready(function () {
  
  dom.selectStatements({
    "hypothesis" : "Предположение"
  });
  
  document.getElementById("value-phi-1").textContent = String(calculator.numbers.formatSmallDouble('0.432332087185902868909253793241999963705110896877651310328152067158553905115295886642477302346753073129013588747517110219254734741730599816815325253701028468603192460457044667286022488406793620201938436437987929552467861296097638935269402775223197319784586355957940362020663386336545448951089096597158627873325857636862001836799521280878650437946101266432604225264008225526752215113354170378353194718394445355780270720570478987039823872288416801432939136351344262772005328157219739104248965032034785'));
  
  //
  
  const ZETA_ACCURACY = 10;
  let zetasFrac = dictionary.ab.ZBest(1, ZETA_ACCURACY, -1) / dictionary.ab.ZBest(0, ZETA_ACCURACY, -1);
  
  document.getElementById("value-phi-2").textContent = String(calculator.numbers.formatSmallDouble(zetasFrac));
  
  //
  
  const MAGIC_CONST = 31 * 83 * 131 * 9883 * 257287 / Math.pow(10, 15);
  const E_ACCURACY = 50;
  let eProduct = Array(E_ACCURACY).fill(0).map((n, i) => 1 - Math.pow(Math.E, -(i + 1))).reduce((a, b) => a * b, 1);
  let phiThrowE1 = MAGIC_CONST * eProduct;
  
  document.getElementById("value-phi-3").textContent = String(calculator.numbers.formatSmallDouble(phiThrowE1));
  
  //
  
  let phiThrowE2 = (1 - Math.pow(Math.E, -2)) / 2;
  
  document.getElementById("value-phi-4").textContent = String(calculator.numbers.formatSmallDouble(phiThrowE2));
  
  /*
  //
  
  var LIMIT = 4;
  var n_list = dictionary.arrays.integers(LIMIT, 1);
  var A_list = n_list.slice().map(function (n) {
    var A_n = dictionary.ab.getA(n, 'recursion');
    return calculator.numbers.formatBigInt(A_n);
  });
  var A_DH_list = n_list.slice().map(function (n) {
    var A_n = dictionary.ab.getA(n, 'DH');
    return calculator.numbers.formatBigInt(A_n);
  });
  var A_Z_list = n_list.slice().map(function (n) {
    var A_n = dictionary.ab.getA(n, 'Z');
    return calculator.numbers.formatBigInt(A_n);
  });
  var B_list = n_list.slice().map(function (n) {
    var B_n = dictionary.ab.getB(n, 'recursion');
    return calculator.numbers.formatBigInt(B_n);
  });
  var B_DH_list = n_list.slice().map(function (n) {
    var B_n = dictionary.ab.getB(n, 'DH');
    return calculator.numbers.formatBigInt(B_n);
  });
  var B_Z_list = n_list.slice().map(function (n) {
    var B_n = dictionary.ab.getB(n, 'Z');
    return calculator.numbers.formatBigInt(B_n);
  });
  
  dom.constructTableFor(
    ['n', 'A[n]', 'A_DH[n]', 'A_Z[n]', 'B[n]', 'B_DH[n]', 'B_Z[n]'],
    [n_list, A_list, A_DH_list, A_Z_list, B_list, B_DH_list, B_Z_list]
  ).appendTo("body");
  
  $("<br/>").appendTo("body");
  
  //
  
  var LIMIT = 4;
  var LN = dictionary.arrays.integers(2 * LIMIT).map(function (i) {
    return { 'l' : 1 - parseInt(i / LIMIT), 'n' : (i % LIMIT) + 1 };
  })
  var ZERO_ROW = ['k\\(l,n)'].concat(LN.map(function (pair) { return '(' + pair['l'] + ', ' + pair['n'] + ')' }));
  var ZERO_COLUMN = dictionary.arrays.integers(Math.pow(2, LIMIT));
  var table = [ ZERO_COLUMN ];
  for (var i = 0; i < LN.length; i++) {
    var l = LN[i]['l'];
    var n = LN[i]['n'];
    var column = [];
    for (var k = 0; k < ZERO_COLUMN.length; k++) {
      column.push(dictionary.ab.d(l, n, k) * dictionary.ab.M(l, n, k));
    }
    table.push(column);
  }
  
  dom.constructTableFor(ZERO_ROW, table).appendTo("body");
  
  $("<br/>").appendTo("body");
  
  //
  
  $("<canvas/>").appendTo("body");
  var plot = new Plot(document.getElementsByTagName("canvas")[0], {
    canvas  : {
      width   : 400,
      height  : 400,
      scale   : 40,
    },
    grid    : true,
    subscriptions : {
      mouseCoordinates : (x, y) => console.log('(x, y) = (' + x + ', ' + y + ')'),
    },
  });
  
  var l = 0;
  var n = 60;
  var chiList = Array(n).fill(0).map((a, i) => dictionary.ab.chi(l, i + 1));
  var curveMap = (x) => chiList.map((chi, i) => chi / Math.pow(Math.E, (i + 1) * x)).reduce((a, b) => a + b, 0);
  plot.addCurve(curveMap, {
    color       : '#000000',
    lineWidth   : 1,
    density     : 0.05
  });
  plot.draw();
  */
  
});