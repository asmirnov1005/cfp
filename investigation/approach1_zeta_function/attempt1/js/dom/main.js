var dom = new (function Dom() {
  
  this.selectStatements = function (statementTypes) {
    $(Object.keys(statementTypes).map(function (statementType) {
      return "." + statementType;
    }).join(",")).each(function (i, statementElement) {
      var elem = $(statementElement);
      var content = elem.html();
      
      elem.html("");
      elem.append(getLabel()).append(getContent());
      elem.addClass("statement");
      
      function getLabel() {
        return $("<div>").addClass("label").html(statementTypes[statementElement.classList[0]]);
      }
      
      function getContent() {
        return $("<div>").addClass("content").html(content);
      }
    });
  }
  
  this.constructTableFor = function (header, columns = []) {
    function createRow(rowValues) {
      function createCell(cellValue) {
        return $("<td>").html(cellValue);
      }
      
      var tableRow = $("<tr>");
      
      for (var i in rowValues)
        tableRow.append(createCell(rowValues[i]));
      
      return tableRow;
    }
    
    var tableElement = $("<table>");
    
    if (header.length != columns.length)
      return tableElement;
    if (dictionary.sets.create(columns.map(function (column) { return column.length })).size() > 1)
      return tableElement;
    if (columns[0].length == 0)
      return tableElement;
    
    tableElement.append(createRow(header));
    for (var i = 0; i < columns[0].length; i++) {
      var rowValues = Array.apply(null, {length : header.length})
                           .map(Function.call, Number)
                           .map(function (j) { return columns[j][i] });
      tableElement.append(createRow(rowValues));
    }
    
    return tableElement;
  }
  
})();