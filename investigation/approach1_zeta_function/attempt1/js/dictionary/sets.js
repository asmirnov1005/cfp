var dictionaryModuleSets = new (function DictionaryModuleSets() {
  
  function Set(elems = []) {
    
    var setSelf;
    
    function removeDuplicates() {
      var elemsWithoutDuplicates = [];
      
      for (var i in elems)
        if (!~elemsWithoutDuplicates.indexOf(elems[i]))
          elemsWithoutDuplicates.push(elems[i]);
      elems = elemsWithoutDuplicates;
    }
    
    this.add = function (elem) {
      if (!~elems.indexOf(elem))
        elems.push(elem);
      
      return setSelf;
    }
    
    this.size = function () {
      return elems.length;
    }
    
    this.show = function () {
      return elems.slice();
    }
    
    this.filter = function (filterCallback) {
      return new Set(elems.filter(filterCallback));
    }
    
    this.init = function () {
      setSelf = this;
      
      removeDuplicates();
    }
    
    this.init();
    
  }
  
  var self;
  
  this.create = function (elems = []) {
    return new Set(elems);
  }
  
  this.S = function (n) {
    if (n == 0)
      return self.create([ [0] ]);
    
    var S = self.create();
    
    for (var i = 0; i < Math.pow(2, n); i++) {
      var bin = i.toString(2);
      bin = '0'.repeat(n - bin.length) + bin;
      S.add(bin.split('').map(j => parseInt(j)));
    }
    
    return S;
  }
  
  this.T = function (n) {
    if (n == 0)
      return self.S(0);
    
    return self.S(n).filter(function (elem) {
      return elem[n - 1] == 1;
    });
  }
  
  this.init = function () {
    self = this;
  }
  
  this.init();
  
})();