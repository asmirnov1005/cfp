var dictionaryModuleAb = new (function DictionaryModuleAb() {
  
  var self;
  
  var zetaComponents = {};
  
  function calculateByRecursion(n, valueFor) {
    var PRIMES = calculator.primes.firstNPrimes(n);
    var PHI = calculator.continuedFractionFor([0].concat(PRIMES));
    
    if (valueFor == 'A')
      return PHI.getBestConvergent().numerator;
    else
      return PHI.getBestConvergent().denominator;
  }
  
  function calculateByDWithHypothesis(n, valueFor) {
    return valueFor == 'A' ? self.D(1, n) : self.D(0, n);
  }
  
  function calculateByZeta(n, valueFor) {
    if (n == 1) return calculateByRecursion(1, valueFor);
    
    return valueFor == 'A' ?
           parseInt(self.Z(1, n, -1)) - calculateByRecursion(n - 1, valueFor) :
           parseInt(self.Z(0, n, -1)) - calculateByRecursion(n - 1, valueFor);
  }
  
  function calculateFor(n, howToCalculate, valueFor) {
    switch (howToCalculate) {
      case 'recursion':
        return calculateByRecursion(n, valueFor);
      case 'D':
        return calculateByD(n, valueFor);
      case 'DH':
        return calculateByDWithHypothesis(n, valueFor);
      case 'Z':
        return calculateByZeta(n, valueFor);
      default:
        return -1;
    }
  }
  
  this.getA = function (n, howToCalculate = 'recursion') {
    return calculateFor(n, howToCalculate, 'A');
  }
  
  this.getB = function (n, howToCalculate = 'recursion') {
    return calculateFor(n, howToCalculate, 'B');
  }
  
  this.D = function (l, n) {
    var result = 0;
    
    for (var k = 0; k <= self.degF(l, n); k++)
      result += self.d(l, n, k) * self.M(l, n, k);
    
    return parseInt(result);
  }
  
  this.degF = function (l, n) {
    if (l >= n - 1)
      return 0;
    
    return Math.round(Math.pow(2, n - (l + 1))) - 1;
  }
  
  this.d = function (l, n, k) {
    if (k < 0)
      return 0;
    
    var ordMinusSum = 0;
    for (var i = 1; i <= k; i++)
      ordMinusSum += calculator.primes.ord(2, i);
    var ordPlusSum = 0;
    for (var i = 2 * k + 1; i <= 3 * k; i++)
      ordPlusSum += calculator.primes.ord(2, i);
    
    if (ordPlusSum > ordMinusSum)
      return 0;
    else
      return 1;
  }
  
  this.delta = function (l, n) {
    var nu = calculator.primes.nu(n);

    return self.d(l, nu, self.k(l, nu, n));
  }
  
  this.M = function (l, n, k) {
    var PRIMES = calculator.primes.firstNPrimes(n);
    var result = 1;
    
    for (var i = l + 1; i <= n; i++)
      result *= Math.pow(PRIMES[i - 1], 1 - self.b(i - (l + 1), k) - self.b(i - (l + 2), k));
    
    return Math.round(result);
  }
  
  this.b = function (i, k) {
    return parseInt(k / Math.pow(2, i)) % 2;
  }
  
  this.inverseM = function (l, n, value) {
    var PRIMES = calculator.primes.firstNPrimes(n);
    var valuations = PRIMES.map(function (p) { return calculator.primes.ord(p, value) });
    
    var k = 0;
    for (var i = 0; i <= n - (l + 1); i++) {
      var b = (i + 1) % 2;
      for (var j = 1; j <= i + 1; j++)
        b += Math.pow(-1, i + j) * valuations[l + j - 1];
      k += b * Math.pow(2, i);
    }
    
    return k;
  }
  
  this.Z = function (l, n, s) {
    var PRIMES = calculator.primes.firstNPrimes(n);
    var LIMIT = PRIMES.reduce((p, q) => p * q);
    var Z = 0;
    
    for (var N = 1; N <= LIMIT; N++) {
      var nu = calculator.primes.nu(N);
      if (nu > n)
        continue;
      if (calculator.primes.mu(N) == 0)
        continue;
      if (self.delta(l, N) == 0)
        continue;
      Z += Math.pow(N, -s);
    }
    
    return Z;
  }
  
  this.k = function (l, n, value) {
    var p = calculator.primes.firstNPrimes(n);
    
    if (l >= n)
      if (value == 1)
        return 0;
      else
        return -1;
    for (var i = 1; i <= l; i++)
      if (calculator.primes.ord(p[i - 1], value) > 0)
        return -1;
    
    var k = 0;
    for (var i = 0; i <= n - (l + 1); i++) {
      var b = (i + 1) % 2;
      for (var j = 1; j <= i + 1; j++)
        b += Math.pow(-1, i + j) * calculator.primes.ord(p[l + j - 1], value);
      k += (b % 2) * Math.pow(2, i);
    }
    
    return k;
  }
  
  this.chi = function (l, n) {
    return Math.abs(calculator.primes.mu(n)) * self.delta(l, n);
  }
  
  this.ZBestComponents = function (l, n) {
    var PRIMES = calculator.primes.firstNPrimes(n);
    var result = [];
    
    if (!~Object.keys(zetaComponents).indexOf(l))
      zetaComponents[l] = Array(l).fill(0).concat([1, PRIMES[l]]).map((number) => [ number ]);
    
    for (var i = zetaComponents[l].length; i <= n; i++)
      zetaComponents[l][i] = zetaComponents[l][i - 2].concat(zetaComponents[l][i - 1].map((number) => number * PRIMES[i - 1]));
    
    result = result.concat(zetaComponents[l][n - 1]);
    result = result.concat(zetaComponents[l][n]);
    
    return result.sort((N, M) => N - M);
  }
  
  this.ZBest = function (l, n, s) {
    return this.ZBestComponents(l, n).reduce((Z, N) => Z + Math.pow(N, -s), 0);
  }
  
  this.ZBestString = function (l, n, s) {
    var PLUS_SYMBOL = " + ";
    var deg = s > 0 ? "^(-" + s.toString() + ")" : "^" + Math.abs(s).toString();
    var zBestComponents = this.ZBestComponents(l, n);
    
    if (s == 0)
      return zBestComponents.length.toString();
    else if (s == -1)
      return zBestComponents.join(PLUS_SYMBOL);
    else
      return zBestComponents.map((N) => N.toString() + deg).join(PLUS_SYMBOL);
  }
  
  this.init = function () {
    self = this;
  }
  
  this.init();
  
})();