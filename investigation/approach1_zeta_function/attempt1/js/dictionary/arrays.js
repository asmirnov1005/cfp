var dictionaryModuleArrays = new (function DictionaryModuleArrays() {
  
  var self;
  
  this.integers = function (length, shift = 0) {
    return Array.apply(null, { length : length }).map(function (blank, i) { return i + shift });
  }
  
  this.init = function () {
    self = this;
  }
  
  this.init();
  
})();