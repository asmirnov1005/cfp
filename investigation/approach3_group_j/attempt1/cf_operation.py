import sys; sys.path.append('../..')

import sympy as sp

import common.continued_fractions as cf

def get_cf_operation_p(f_p, g_p, p):
    f = f_p(p)
    g = g_p(p)
    def operation_p_func(a, b):
        return f(g(a) + g(b))
    return operation_p_func

def get_cf_operation(operation_p, x, y, n=0):
    def cf_operation(x_arr, y_arr):
        if len(x_arr) == 0 or len(y_arr) == 0:
            return []
        if x_arr[0] != 0:
            raise ValueError('Bad value of x_arr')
        if y_arr[0] != 0:
            raise ValueError('Bad value of y_arr')
        m = min(len(x_arr), len(y_arr)) - 1
        if m == 0:
            return [0]
        return [0] + [operation_p(p)(x_arr[i + 1], y_arr[i + 1]) for (i, p) in enumerate(sp.primerange(2, sp.prime(m) + 1))]
    def mixed_operation(x_arr, y_real):
        y_arr = cf.continued_fraction(y_real, len(x_arr))
        return cf_operation(x_arr, y_arr)
    def r_operation(x_real, y_real):
        x_arr = cf.continued_fraction(x_real, n)
        y_arr = cf.continued_fraction(y_real, n)
        return cf_operation(x_arr, y_arr)
    if isinstance(x, list) and isinstance(y, list):
        return cf_operation(x, y)
    elif isinstance(x, list) and not isinstance(y, list):
        return mixed_operation(x, y)
    elif not isinstance(x, list) and isinstance(y, list):
        return mixed_operation(y, x)
    else:
        return r_operation(x, y)