import sys; sys.path.append('../..')

from math import log

import cf_operation as cfo
from common.z_p_inf import ZpInf

def alpha2_p(p):
    F = ZpInf(p)
    def alpha_p_func(n):
        if n < 1:
            raise ValueError("Bad value of n")
        if n == 1:
            return F.unit()
        else:
            N = int(log(n - 1, p))
            return F.get(n - p**N + int((n - p**N - 1) / (p - 1)), N + 1)
    return alpha_p_func

def beta2_p(p):
    F = ZpInf(p)
    def beta_p_func(x):
        if not x in F:
            raise ValueError("Bad value of x")
        if x.m == 0:
            return 1
        else:
            return p**(x.n - 1) + x.m - int(x.m / p)
    return beta_p_func

def odot2_p(p):
    return cfo.get_cf_operation_p(beta2_p, alpha2_p, p)

def odot2(x, y, n=0):
    return cfo.get_cf_operation(odot2_p, x, y, n)