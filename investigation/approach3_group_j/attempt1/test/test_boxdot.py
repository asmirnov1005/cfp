import sys; sys.path.append('..'); sys.path.append('../../..')

from pytest import raises

import common.continued_fractions as cf
import boxdot as bd

def test_xi_p():
    with raises(ValueError):
        bd.xi_p(1)
    xi_2 = bd.xi_p(2)
    with raises(ValueError):
        xi_2(0)
    assert xi_2(1) == -1
    assert xi_2(2) == 0
    assert xi_2(3) == -2
    assert xi_2(4) == 1
    assert xi_2(5) == -3
    assert xi_2(6) == 2
    assert xi_2(7) == -4
    assert xi_2(8) == 3
    assert xi_2(9) == -5
    assert xi_2(10) == 4
    xi_5 = bd.xi_p(5)
    assert xi_5(1) == -1
    assert xi_5(2) == -2
    assert xi_5(3) == -3
    assert xi_5(4) == -4
    assert xi_5(5) == 0
    assert xi_5(6) == -5
    assert xi_5(7) == -6
    assert xi_5(8) == -7
    assert xi_5(9) == -8
    assert xi_5(10) == 1

def test_eta_p():
    with raises(ValueError):
        bd.eta_p(1)
    eta_2 = bd.eta_p(2)
    assert eta_2(-5) == 9
    assert eta_2(-4) == 7
    assert eta_2(-3) == 5
    assert eta_2(-2) == 3
    assert eta_2(-1) == 1
    assert eta_2(0) == 2
    assert eta_2(1) == 4
    assert eta_2(2) == 6
    assert eta_2(3) == 8
    assert eta_2(4) == 10
    assert eta_2(5) == 12
    eta_5 = bd.eta_p(5)
    assert eta_5(-5) == 6
    assert eta_5(-4) == 4
    assert eta_5(-3) == 3
    assert eta_5(-2) == 2
    assert eta_5(-1) == 1
    assert eta_5(0) == 5
    assert eta_5(1) == 10
    assert eta_5(2) == 15
    assert eta_5(3) == 20
    assert eta_5(4) == 25
    assert eta_5(5) == 30

def test_boxdot_p():
    with raises(ValueError):
        bd.boxdot_p(1)
    boxdot_2 = bd.boxdot_p(2)
    with raises(ValueError):
        boxdot_2(0, 1)
    with raises(ValueError):
        boxdot_2(1, 0)
    with raises(ValueError):
        boxdot_2(0, 0)
    assert boxdot_2(1, 1) == 3
    assert boxdot_2(1, 2) == 1
    assert boxdot_2(1, 3) == 5
    assert boxdot_2(2, 1) == 1
    assert boxdot_2(2, 2) == 2
    assert boxdot_2(2, 3) == 3
    assert boxdot_2(3, 1) == 5
    assert boxdot_2(3, 2) == 3
    assert boxdot_2(3, 3) == 7
    boxdot_5 = bd.boxdot_p(5)
    assert boxdot_5(1, 1) == 2
    assert boxdot_5(1, 2) == 3
    assert boxdot_5(1, 3) == 4
    assert boxdot_5(2, 1) == 3
    assert boxdot_5(2, 2) == 4
    assert boxdot_5(2, 3) == 6
    assert boxdot_5(3, 1) == 4
    assert boxdot_5(3, 2) == 6
    assert boxdot_5(3, 3) == 7

def test_boxdot():
    with raises(ValueError):
        bd.boxdot([0, 1], [1, 1])
    x_arr = [0, 2, 3, 5, 7, 11, 13]
    y_arr = [0, 1, 1, 1, 1, 1, 1, 1]
    z_arr = bd.boxdot(x_arr, y_arr)
    z_arr_expected = [0, 1, 1, 1, 1, 1, 1]
    assert len(z_arr) == len(z_arr_expected)
    for i in range(len(z_arr)):
        assert z_arr[i] == z_arr_expected[i]
    y_real = 0.3
    z_arr = bd.boxdot(x_arr, y_real)
    z_arr_expected = cf.continued_fraction(y_real, 3)
    assert len(z_arr) == len(z_arr_expected)
    for i in range(len(z_arr)):
        assert z_arr[i] == z_arr_expected[i]
    x_real = 0.432332087
    z_arr = bd.boxdot(x_real, y_real, 3)
    z_arr_expected = cf.continued_fraction(y_real, 3)
    assert len(z_arr) == len(z_arr_expected)
    for i in range(len(z_arr)):
        assert z_arr[i] == z_arr_expected[i]