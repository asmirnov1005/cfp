import sys; sys.path.append('..'); sys.path.append('../../..')

from pytest import raises

import common.continued_fractions as cf
from common.z_p_inf import ZpInf
import odot as od

def test_alpha_p():
    with raises(ValueError):
        od.alpha_p(1)
    p = 2
    F_2 = ZpInf(p)
    alpha_2 = od.alpha_p(p)
    with raises(ValueError):
        alpha_2(0)
    assert alpha_2(1) == F_2.unit()
    assert alpha_2(2) == F_2.get(1, 1)
    assert alpha_2(3) == F_2.get(1, 2)
    assert alpha_2(4) == F_2.get(3, 2)
    assert alpha_2(5) == F_2.get(1, 3)
    assert alpha_2(6) == F_2.get(5, 3)
    assert alpha_2(7) == F_2.get(3, 3)
    assert alpha_2(8) == F_2.get(7, 3)
    q = 5
    F_5 = ZpInf(q)
    alpha_5 = od.alpha_p(q)
    assert alpha_5(1) == F_5.unit()
    assert alpha_5(2) == F_5.get(1, 1)
    assert alpha_5(3) == F_5.get(2, 1)
    assert alpha_5(4) == F_5.get(3, 1)
    assert alpha_5(5) == F_5.get(4, 1)
    assert alpha_5(6) == F_5.get(1, 2)
    assert alpha_5(7) == F_5.get(6, 2)
    assert alpha_5(8) == F_5.get(7, 2)
    assert alpha_5(9) == F_5.get(8, 2)
    assert alpha_5(10) == F_5.get(9, 2)
    assert alpha_5(11) == F_5.get(2, 2)

def test_beta_p():
    with raises(ValueError):
        od.beta_p(1)
    p = 2
    F_2 = ZpInf(p)
    beta_2 = od.beta_p(p)
    assert beta_2(F_2.get(0, 1)) == 1
    assert beta_2(F_2.get(1, 1)) == 2
    assert beta_2(F_2.get(1, 2)) == 3
    assert beta_2(F_2.get(3, 2)) == 4
    assert beta_2(F_2.get(1, 3)) == 5
    assert beta_2(F_2.get(3, 3)) == 7
    assert beta_2(F_2.get(5, 3)) == 6
    assert beta_2(F_2.get(7, 3)) == 8
    q = 5
    F_5 = ZpInf(q)
    beta_5 = od.beta_p(q)
    assert beta_5(F_5.get(0, 1)) == 1
    assert beta_5(F_5.get(1, 1)) == 2
    assert beta_5(F_5.get(2, 1)) == 3
    assert beta_5(F_5.get(3, 1)) == 4
    assert beta_5(F_5.get(4, 1)) == 5
    assert beta_5(F_5.get(1, 2)) == 6
    assert beta_5(F_5.get(2, 2)) == 11
    assert beta_5(F_5.get(3, 2)) == 16
    assert beta_5(F_5.get(4, 2)) == 21
    assert beta_5(F_5.get(6, 2)) == 7
    assert beta_5(F_5.get(7, 2)) == 8
    assert beta_5(F_5.get(8, 2)) == 9
    assert beta_5(F_5.get(9, 2)) == 10

def test_odot_p():
    with raises(ValueError):
        od.odot_p(1)
    odot_2 = od.odot_p(2)
    with raises(ValueError):
        odot_2(0, 1)
    with raises(ValueError):
        odot_2(1, 0)
    with raises(ValueError):
        odot_2(0, 0)
    assert odot_2(1, 1) == 1
    assert odot_2(1, 2) == 2
    assert odot_2(1, 3) == 3
    assert odot_2(2, 1) == 2
    assert odot_2(2, 2) == 1
    assert odot_2(2, 3) == 4
    assert odot_2(3, 1) == 3
    assert odot_2(3, 2) == 4
    assert odot_2(3, 3) == 2
    odot_5 = od.odot_p(5)
    assert odot_5(1, 1) == 1
    assert odot_5(1, 2) == 2
    assert odot_5(1, 3) == 3
    assert odot_5(2, 1) == 2
    assert odot_5(2, 2) == 3
    assert odot_5(2, 3) == 4
    assert odot_5(3, 1) == 3
    assert odot_5(3, 2) == 4
    assert odot_5(3, 3) == 5

def test_odot():
    with raises(ValueError):
        od.odot([0, 1], [1, 1])
    x_arr = [0, 2, 3, 5, 7, 11, 13]
    y_arr = [0, 2, 2, 2, 2, 2, 2, 2]
    z_arr = od.odot(x_arr, y_arr)
    z_arr_expected = [0, 1, 1, 1, 1, 1, 1]
    assert len(z_arr) == len(z_arr_expected)
    for i in range(len(z_arr)):
        assert z_arr[i] == z_arr_expected[i]
    y_real = 0.3
    z_arr = od.odot(x_arr, y_real)
    z_arr_expected = [0, 4, 1, 5]
    assert len(z_arr) == len(z_arr_expected)
    for i in range(len(z_arr)):
        assert z_arr[i] == z_arr_expected[i]
    x_real = 0.432332087
    z_arr = od.odot(x_real, y_real, 3)
    z_arr_expected = [0, 4, 1, 5]
    assert len(z_arr) == len(z_arr_expected)
    for i in range(len(z_arr)):
        assert z_arr[i] == z_arr_expected[i]