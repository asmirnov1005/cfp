import sys; sys.path.append('../..')

from math import gcd, log

import cf_operation as cfo
from common.z_p_inf import ZpInf

def v_p(p, n):
    import sympy as sp
    if not sp.isprime(p):
        raise ValueError("Bad value of p")
    if not isinstance(n, int) or n < 1:
        raise ValueError("Bad value of n")
    d = 0
    while gcd(n, p**d) == p**d:
        d += 1
    return d - 1

def alpha_p(p):
    F = ZpInf(p)
    def alpha_p_func(n):
        if n < 1:
            raise ValueError("Bad value of n")
        if n == 1:
            return F.unit()
        else:
            return F.get(n - 1, 1 + v_p(p, n - 1) + int(log(n - 1, p)))
    return alpha_p_func

def beta_p(p):
    F = ZpInf(p)
    def beta_p_func(x):
        if not x in F:
            raise ValueError("Bad value of x")
        if x.m == 0:
            return 1
        else:
            return 1 + x.m * p**(x.n - int(log(x.m, p)) - 1)
    return beta_p_func

def odot_p(p):
    return cfo.get_cf_operation_p(beta_p, alpha_p, p)

def odot(x, y, n=0):
    return cfo.get_cf_operation(odot_p, x, y, n)