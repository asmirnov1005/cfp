from math import floor

import sympy as sp

import cf_operation as cfo

def xi_p(p):
    if not sp.isprime(p):
        raise ValueError('Bad value of p')
    def xi_p_func(n):
        if n < 1:
            raise ValueError('Bad value of n')
        if n % p == 0:
            return int(n / p - 1)
        else:
            return int(floor(n / p) - n)
    return xi_p_func

def eta_p(p):
    if not sp.isprime(p):
        raise ValueError('Bad value of p')
    def eta_p_func(m):
        if m < 0:
            return int((1 + m * p) / (1 - p))
        else:
            return (1 + m) * p
    return eta_p_func

def boxdot_p(p):
    return cfo.get_cf_operation_p(eta_p, xi_p, p)

def boxdot(x, y, n=0):
    return cfo.get_cf_operation(boxdot_p, x, y, n)