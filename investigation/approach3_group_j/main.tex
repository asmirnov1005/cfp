\documentclass[14pt]{extarticle}

\usepackage[T2A]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[russian,english]{babel}

\usepackage{amsmath, amsthm, amsfonts, amssymb}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=[RGB]{188,128,189},
    anchorcolor=black,
    citecolor=green,
    filecolor=cyan,      
    menucolor=red,
    runcolor=cyan,
    urlcolor=[RGB]{31,120,180},
}

\newtheorem{guess}{Предположение}

\setlength\parindent{0pt}

\title{Групповая структура на $J$}
\author{}
\date{}

\begin{document}

\maketitle

\begin{section}{Общие обозначения и определения}

Пусть ${ \mathbb{N} := \mathbb{Z}_{> 0} }$.

Пусть ${ J := [0, 1] \cap (\mathbb{R} \setminus \mathbb{Q}) }$.

Любой элемент ${ x \in J }$ можно единственным образом представить в виде непрерывной дроби ${ x = [0; a_1, a_2, a_3, ...] }$. Также это значит, что элементы $J$ взаимно однозначно соответствуют функциям ${ f_x : \mathbb{N} \rightarrow \mathbb{N} }$.

Пусть ${ x_n = [0; n, n, n, ...] }$, то есть, ${ f_{x_n}(m) = n }$ для любого ${ m \in \mathbb{N} }$. Тогда

\begin{equation*}
    x_n = \sqrt{\left(\frac{n}{2}\right)^2 + 1} - \frac{n}{2}.
\end{equation*}

Пусть ${ x_e := e - 2 \in J }$, ${ x_\pi := \pi - 3 \in J }$.

Для любого ${ x \in \mathbb{R} }$ пусть

\begin{gather*}
    \rho(x) :=
    \{ x \} =
    \frac{1}{2} - \frac{1}{\pi} \sum_{k = 1}^\infty \frac{\sin(2 \pi k x)}{k}, \\
    \theta(x) :=
    \rho(x)^{-1}, \\
    \sigma(x) :=
    \lfloor x \rfloor =
    x - \rho(x) =
    x - \frac{1}{\theta(x)}.
\end{gather*}

Тогда если ${ x \in J }$ имеет разложение ${ [0; a_1, a_2, ...] }$, то ${ a_n = \sigma(\theta^n(x)) }$, где ${ \theta^n }$ означает $n$-кратное применение функции $\theta$.

Групповую структуру $\circledast$ на множестве $J$ я назову CFR-инвариантной (CFR = continued fraction representation), если существует набор групповых структур $\left\{ (\mathbb{N}, \circledast_n) \right\}_{n \in \mathbb{N}}$, такой, что для любых ${ x, y \in J }$, имеющих разложения ${ x = [0; a_1, a_2, ...] }$ и ${ y = [0; b_1, b_2, ...] }$ соответственно, выполняется:

\begin{equation*}
    x \circledast y = [0; a_1 \circledast_1 b_1, a_2 \circledast_2 b_2, ...].
\end{equation*}

Иначе говоря, для любых ${ f_x, f_y \in J }$ операция $\circledast$ представима в виде

\begin{equation*}
    f_x \circledast f_y : n \mapsto f_x(n) \circledast_n f_y(n).
\end{equation*}

\end{section}

\begin{section}{Описание идеи}

Задать на множестве $J$ групповую структуру\footnote{
    Это также может быть полугруппа, модуль, кольцо, топологическая группа и т.д.
}, а потом воспользоваться этой структурой для вычисления $\Phi$.

\end{section}

\begin{section}{Первая попытка}

Попробовал задать групповую операцию с помощью биекции между $\mathbb{N}$ и некоторой группой.

Например, пусть есть биекция ${ f : \mathbb{N} \rightarrow (G, +_G) }$, тогда ${ (\mathbb{N}, *) }$ --- группа, где

\begin{equation*}
    a * b := f^{-1}(f(a) +_G f(b)).
\end{equation*}

Далее, нужно смотреть на $J$ как на ${ \prod\limits_i \mathbb{N} }$, и для каждой $i$-й компоненты выбрать некоторую биекцию $f_i$ с группой $G_i$, порождающую операцию $*_i$. В итоге,

\begin{equation*}
    [0; a_1, a_2, ...] * [0; b_1, b_2, ...] = [0; a_1 *_1 b_1, a_2 *_2 b_2, ...].
\end{equation*}

Такая групповая структура заведомо (по построению) получается CFR-инвариантной.

Таким образом я попробовал определить две группы. Я занумеровал компоненты простыми числами, и построил следующие биекции:

\begin{itemize}
    \item ${ \xi_p :
      \mathbb{N} \rightarrow \mathbb{Z},
      n \mapsto \begin{cases}
        \begin{matrix}
          \frac{n}{p} - 1, &
          \text{если $p | n$,} \\
          \left\lfloor \frac{n}{p} \right\rfloor - n, &
          \text{иначе;}
        \end{matrix}
      \end{cases} }$
    \item ${ \alpha_p :
      \mathbb{N} \rightarrow \mathbb{Z}(p^\infty),
      n \mapsto \frac{n - 1}{p^{1 + v_p(n - 1) + \lfloor \log_p(n - 1) \rfloor}}. }$
\end{itemize}

Под $\mathbb{Z}(p^\infty)$ подразумевается квазициклическая $p$-группа ($p$-группа Прюфера):

\begin{equation*}
  \mathbb{Z}(p^\infty) =
  \left\{
    \frac{m}{p^n} \in [0, 1) \cap \mathbb{Q}
  \middle|
    m, n \in \mathbb{Z}_{\geq 0}, p \nmid m
  \right\},
\end{equation*}

где групповой операцией будет сложение дробей (с сокращением) по модулю целых чисел.

Однако, пока что не удалось получить никаких результатов, которые помогли бы вычислить значение $\Phi$.

\end{section}

\begin{section}{Вторая попытка}

Во второй попытке я пытаюсь исповедовать наиболее общий подход --- рассмотреть категорию сразу всех CFR-инвариантных групповых структур.

Пока что, в этом направлении почти ничего не сделано.

\end{section}

\begin{section}{Третья попытка}

Пытаюсь добавить к групповой структуре топологическую, и, заодно, привязать к ней меру Хаара.

Пока что никаких хороших результатов не получено.

\end{section}

\end{document}