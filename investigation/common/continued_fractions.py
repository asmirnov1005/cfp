from math import e, pi, sqrt, floor

x_e = e - 2
x_pi = pi - 3

def x_n(n):
    if n < 1:
        raise ValueError('Bad value of n')
    return sqrt((n / 2.0)**2 + 1) - n / 2.0

def sigma(x):
    return floor(x)

def rho(x):
    return x - sigma(x)

def theta(x, n=0):
    if not isinstance(x, int) and not isinstance(x, float):
        raise ValueError('Bad value of x')
    if n < 0:
        raise ValueError('Bad value of n')
    if n == 0:
        return x
    if abs(rho(theta(x, n - 1))) < 10**-9:
        return 0.0
    return 1.0 / rho(theta(x, n - 1))

def f_x(x, n):
    return sigma(theta(x, n))

def continued_fraction(x, n=0):
    cf = [f_x(x, m) for m in range(n + 1)]
    return [cf[0]] + list(filter(lambda a: a > 0, cf[1:]))

def convergent(x, n=0):
    if not isinstance(x, list):
        cf = continued_fraction(x, n)
        return convergent(cf, len(cf) - 1)
    if len(x) < n + 1:
        raise ValueError('Bad value of n')
    p1, p2 = x[0], 1
    q1, q2 = 1, 0
    if n == -1:
        return (p2, q2)
    for i in range(1, n + 1):
        p1, p2 = x[i] * p1 + p2, p1
        q1, q2 = x[i] * q1 + q2, q1
    return (p1, q1)