class ZpInf:
    import sympy as _sp

    def __init__(self, p):
        if not self._sp.isprime(p):
            raise ValueError("p should be prime, but p == {0}".format(p))
        self.p = p

    def get(self, m, n):
        return self._ZpInfElem(self.p, m, n)

    def unit(self):
        return self._ZpInfElem(self.p, 0, 1)

    def __contains__(self, item):
        return isinstance(item, ZpInf._ZpInfElem) and self.p == item.p

    def __repr__(self):
        return "<Z[{p}**inf]>".format(p=self.p)

    class _ZpInfElem:
        import math as _m

        def __init__(self, p, m, n):
            if not isinstance(n, int):
                raise ValueError("n should be int but type(n)={0}".format(type(n)))
            if n < 1:
                raise ValueError("n should be strictly positive, but n={0}".format(n))
            if not isinstance(m, int):
                raise ValueError("m should be int but type(m)={0}".format(type(m)))
            if m < 0:
                raise ValueError("m should be positive, but m={0}".format(m))
            if m >= p**n:
                raise ValueError("m should be <{0}^{1}(={2}), but m={3}".format(p, n, p**n, m))
            self.p = p
            if m == 0:
                self.m = 0
                self.n = 1
            else:
                d = int(self._m.log(self._m.gcd(m, self.p**n), self.p))
                self.m = int(m / (p**d))
                self.n = n - d

        def __add__(self, other):
            self._check_other(other)
            n = max(self.n, other.n)
            m = self.m * self.p**(n - self.n) + other.m * other.p**(n - other.n)
            return ZpInf._ZpInfElem(self.p, m % self.p**n, n)

        def __sub__(self, other):
            self._check_other(other)
            n = max(self.n, other.n)
            m = self.m * self.p**(n - self.n) - other.m * other.p**(n - other.n)
            return ZpInf._ZpInfElem(self.p, (self.p**n + m) % self.p**n, n)

        def __eq__(self, other):
            self._check_other(other)
            return self.m == other.m and self.n == other.n

        def __ne__(self, other):
            return not (self == other)

        def __repr__(self):
            if self.m == 0:
                return "<0>"
            elif self.n == 1:
                return "<{m}/{p}>".format(p=self.p, m=self.m)
            else:
                return "<{m}/({p}**{n})>".format(p=self.p, m=self.m, n=self.n)

        def _check_other(self, other):
            if not isinstance(other, ZpInf._ZpInfElem):
                raise ValueError("other summand should be in the same field, but type(other)={0}".format(type(other)))
            if self.p != other.p:
                raise ValueError("other summand should be in the same field, but {0}=p!=other.p={1}".format(self.p, other.p))