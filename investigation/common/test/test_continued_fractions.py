import sys; sys.path.append('..')

from math import e, pi
from pytest import approx, raises

import continued_fractions as cf

def test_sigma():
    assert cf.sigma(1.3) == 1
    assert cf.sigma(-1.3) == -2

def test_rho():
    assert cf.rho(1.3) == approx(.3)
    assert cf.rho(-1.3) == approx(.7)

def test_theta():
    with raises(ValueError):
        cf.theta(1.3, -1)
    with raises(ValueError):
        cf.theta('1.3')
    assert cf.theta(1.3) == approx(1.3)
    assert cf.theta(1.3, 1) == approx(cf.rho(1.3)**(-1))
    assert cf.theta(1.3, 2) == approx(cf.theta(cf.theta(1.3, 1), 1))
    assert cf.theta(1.3, 3) == approx(0)

def test_f_x():
    with raises(ValueError):
        cf.f_x(1.3, -1)
    with raises(ValueError):
        cf.f_x('1.3', 0)
    assert cf.f_x(1.3, 0) == 1
    assert cf.f_x(1.3, 1) == 3
    assert cf.f_x(1.3, 2) == 3
    assert cf.f_x(1.3, 3) == 0

def test_x_e():
    assert cf.x_e == approx(cf.rho(e))

def test_x_pi():
    assert cf.x_pi == approx(cf.rho(pi))

def test_x_n():
    with raises(ValueError):
        cf.x_n(0)
    x_2 = cf.x_n(2)
    assert cf.f_x(x_2, 0) == 0
    for n in range(1, 10):
        assert cf.f_x(x_2, n) == 2

def test_continued_fraction():
    with raises(ValueError):
        cf.continued_fraction('1.3')
    cf_1p3 = cf.continued_fraction(1.3)
    assert len(cf_1p3) == 1
    assert cf_1p3[0] == approx(cf.f_x(1.3, 0))
    cf_1p3_5 = cf.continued_fraction(1.3, 5)
    cf_1p3_5_expected = [1, 3, 3]
    assert len(cf_1p3_5) == len(cf_1p3_5_expected)
    assert all([a == b for a, b in zip(cf_1p3_5, cf_1p3_5_expected)])
    cf_pi_10 = cf.continued_fraction(pi, 10)
    cf_pi_10_expected = [3, 7, 15, 1, 292, 1, 1, 1, 2, 1, 3]
    assert len(cf_pi_10) == len(cf_pi_10_expected)
    assert all([a == b for a, b in zip(cf_pi_10, cf_pi_10_expected)])

def test_convergent():
    with raises(ValueError):
        cf.convergent('1.3')
    assert cf.convergent(1.3) == (1, 1)
    assert cf.convergent(1.3, 2) == (13, 10)
    assert cf.convergent(1.3, 3) == (13, 10)
    assert cf.convergent(pi) == (3, 1)
    assert cf.convergent(pi, 1) == (22, 7)
    assert cf.convergent(pi, 4) == (103993, 33102)
    with raises(ValueError):
        cf.convergent([3, 7], 2)
    assert cf.convergent([3, 7], 1) == (22, 7)
    assert cf.convergent([3, 7, 15, 1, 292], 4) == (103993, 33102)