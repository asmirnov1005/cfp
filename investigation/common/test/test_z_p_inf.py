import sys; sys.path.append('..')

from pytest import raises

from z_p_inf import ZpInf

def test_z_p_inf_init():
    with raises(ValueError):
        ZpInf(-2)
    with raises(ValueError):
        ZpInf(0)
    with raises(ValueError):
        ZpInf(1)
    with raises(ValueError):
        ZpInf(4)
    p = 2
    F = ZpInf(p)
    assert F.p == p

def test_z_p_inf_unit():
    p = 2
    F = ZpInf(p)
    u = F.unit()
    assert u.p == p
    assert u.m == 0
    assert u.n == 1

def test_z_p_inf_contains():
    p = 2
    q = 3
    F = ZpInf(p)
    G = ZpInf(q)
    x = F.get(1, 1)
    y = G.get(1, 1)
    assert x in F
    assert y not in F
    assert 0 not in F
    assert 'x' not in F

def test_z_p_inf_elem_init():
    p = 2
    F = ZpInf(p)
    with raises(ValueError):
        F.get(1, 1.0)
    with raises(ValueError):
        F.get(1, -1)
    with raises(ValueError):
        F.get(1.0, 1)
    with raises(ValueError):
        F.get(-1, 1)
    with raises(ValueError):
        F.get(2, 1)
    u = F.get(0, 5)
    assert u.p == p
    assert u.m == 0
    assert u.n == 1
    x = F.get(1, 1)
    assert x.p == p
    assert x.m == 1
    assert x.n == 1
    y = F.get(6, 3)
    assert y.p == p
    assert y.m == 3
    assert y.n == 2

def test_z_p_inf_elem_add():
    p = 2
    q = 3
    F = ZpInf(p)
    G = ZpInf(q)
    u = F.unit()
    x = F.get(1, 1)
    y = F.get(1, 2)
    z = G.get(1, 1)
    with raises(ValueError):
        x + 0
    with raises(ValueError):
        x + z
    with raises(ValueError):
        z + x
    s1 = x + y
    s2 = y + x
    d1 = x - y
    d2 = y - x
    xs = x + u
    xd = x - u
    nx = u - x
    assert s1.p == p
    assert s1.m == 3
    assert s1.n == 2
    assert s2.p == p
    assert s2.m == 3
    assert s2.n == 2
    assert d1.p == p
    assert d1.m == 1
    assert d1.n == 2
    assert d2.p == p
    assert d2.m == 3
    assert d2.n == 2
    assert xs.p == p
    assert xs.m == x.m
    assert xs.n == x.n
    assert xd.p == p
    assert xd.m == x.m
    assert xd.n == x.n
    assert nx.p == p
    assert nx.m == 1
    assert nx.n == 1

def test_z_p_inf_elem_eq():
    p = 2
    q = 3
    F = ZpInf(p)
    G = ZpInf(q)
    x = F.get(1, 1)
    y = F.get(1, 1)
    z1 = F.get(0, 1)
    z2 = F.get(1, 2)
    z3 = G.get(1, 1)
    with raises(ValueError):
        0 == x
    with raises(ValueError):
        0 != x
    with raises(ValueError):
        x == 0
    with raises(ValueError):
        x != 0
    with raises(ValueError):
        x == z3
    with raises(ValueError):
        x != z3
    with raises(ValueError):
        z3 == x
    with raises(ValueError):
        z3 != x
    assert x == x
    assert x == y
    assert x != z1
    assert x != z2