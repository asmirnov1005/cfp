class SL2Z:
    def get(self, a, b, c, d):
        return self._SL2ZElem(a, b, c, d)

    def unit(self):
        return self._SL2ZElem(1, 0, 0, 1)

    def __contains__(self, item):
        return isinstance(item, SL2Z._SL2ZElem)

    def __repr__(self):
        return "SL[2](Z)"

    class _SL2ZElem:
        def __init__(self, a, b, c, d):
            assert isinstance(a, int) and isinstance(b, int) and isinstance(c, int) and isinstance(d, int), \
                   "a, b, c, d should be int"
            if a * d - b * c != 1:
                raise ValueError("det should be equal to 1")
            self.a = a
            self.b = b
            self.c = c
            self.d = d

        def __mul__(self, other):
            self._check_other(other)
            return SL2Z._SL2ZElem(self.a * other.a + self.b * other.c,
                                  self.a * other.b + self.b * other.d,
                                  self.c * other.a + self.d * other.c,
                                  self.c * other.b + self.d * other.d)

        def __truediv__(self, other):
            self._check_other(other)
            return self * other.inv()

        def __eq__(self, other):
            self._check_other(other)
            return self.a == other.a and self.b == other.b and self.c == other.c and self.d == other.d

        def __ne__(self, other):
            return not (self == other)

        def __repr__(self):
            return "(({a} {b}) ({c} {d}))".format(a=self.a, b=self.b, c=self.c, d=self.d)

        def inv(self):
            return SL2Z._SL2ZElem(self.d, -self.b, -self.c, self.a)

        def apply(self, pair):
            assert isinstance(pair[0], int) and isinstance(pair[1], int), \
                "numerator and denominator should be int"
            return (self.a * pair[0] + self.b * pair[1], self.c * pair[0] + self.d * pair[1])

        def characteristic_polynomial(self):
            b = -(self.a + self.d)
            if b > 0:
                return "x^2 + {b} x + 1".format(b=b)
            else:
                return "x^2 - {b} x + 1".format(b=abs(b))

        def eigenvalues(self):
            from math import sqrt
            b = -(self.a + self.d)
            sqrt_D = sqrt(b**2 - 4)
            return set([(-b + sqrt_D) / 2.0, (-b - sqrt_D) / 2.0])

        def _check_other(self, other):
            if not isinstance(other, SL2Z._SL2ZElem):
                raise ValueError("other summand should be in the same group, but type(other)={0}".format(type(other)))