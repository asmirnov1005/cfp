import sys; sys.path.append('..')

import sympy as sp

import common.continued_fractions as cf

PHI_STR_CONST = "0.43233208718590286890925379324199996370511089688"
PHI_CONST = float(PHI_STR_CONST)

def convergents(n):
    primes = sp.sieve[1 : n + 1].tolist()
    return [tuple(reversed(cf.convergent(primes, i - 1))) for i in range(n)]