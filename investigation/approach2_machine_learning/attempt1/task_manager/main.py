#!/usr/bin/env python3

"""Managing script for the util cfexpr_searcher.
"""

from argparse import ArgumentParser
from subprocess import Popen
from time import sleep
from yaml import load as load_yaml

parser = ArgumentParser(description='Manager of searching')
# Positional Arguments
parser.add_argument('input',
                    help='Path to the yaml file with the searching plan',
                    metavar='PATH_TO_YAML')
# Optional Arguments
parser.add_argument('-p', '--print',
                    help='Specify printing type',
                    required=False,
                    choices=['nothing', 'as-is', 'all'],
                    default='as-is')
args = parser.parse_args()

def prepare_list(arg_value):
    return [str(value) for value in arg_value]

def prepare_bool(arg_value):
    return '1' if arg_value else '0'

def execute_mutations_generator(run_args, print_type, processes):
    args = []
    args.append('cfexpr_searcher/bin/mutations_generator')
    args.extend(['-o', run_args['output']])
    args.extend(['-p', run_args['pattern']])
    args.extend(['-c'] + prepare_list(run_args['constants']))
    if 'buffer-size' in run_args.keys():
        args.extend(['--buffer-size', str(run_args['buffer-size'])])
    if print_type == 'as-is':
        if 'show-percentage' in run_args.keys():
            args.extend(['--show-percentage', prepare_bool(run_args['show-percentage'])])
        if 'show-warnings' in run_args.keys():
            args.extend(['--show-warnings', prepare_bool(run_args['show-warnings'])])
    elif print_type == 'nothing':
        args.extend(['--show-percentage', '0'])
        args.extend(['--show-warnings', '0'])
    elif print_type == 'all':
        args.extend(['--show-percentage', '1'])
        args.extend(['--show-warnings', '1'])
    processes.append(Popen(args))

def execute_cfexpr_searcher(run_args, print_type, processes):
    args = []
    args.append('cfexpr_searcher/bin/cfexpr_searcher')
    args.extend(['-i', run_args['input']])
    args.extend(['-o', run_args['output']])
    args.extend(['-v', str(run_args['target-value'])])
    if 'adam-expr' in run_args.keys():
        args.extend(['-a', run_args['adam-expr']])
    if 'generation-size' in run_args.keys():
        args.extend(['-s', str(run_args['generation-size'])])
    if 'index-names' in run_args.keys():
        args.extend(['--index-names'] + prepare_list(run_args['index-names']))
    if 'index-weights' in run_args.keys():
        args.extend(['--index-weights'] + prepare_list(run_args['index-weights']))
    if 'epsilon' in run_args.keys():
        args.extend(['-e', str(run_args['epsilon'])])
    if 'limit-generation-id' in run_args.keys():
        args.extend(['--limit-generation-id', str(run_args['limit-generation-id'])])
    if 'n-iter-no-improve' in run_args.keys():
        args.extend(['--n-iter-no-improve', str(run_args['n-iter-no-improve'])])
    if 'max-mutations-per-member' in run_args.keys():
        args.extend(['--max-mutations-per-member', str(run_args['max-mutations-per-member'])])
    if 'weak-limit-multiplier' in run_args.keys():
        args.extend(['--weak-limit-multiplier', str(run_args['weak-limit-multiplier'])])
    if print_type == 'as-is' and 'show-processing' in run_args.keys():
        args.extend(['--show-processing', prepare_bool(run_args['show-processing'])])
    elif print_type == 'nothing':
        args.extend(['--show-processing', '0'])
    elif print_type == 'all':
        args.extend(['--show-processing', '1'])

    processes.append(Popen(args))

def execute(run_args, print_type, processes):
    if run_args['script'] == 'mutations_generator':
        execute_mutations_generator(run_args, print_type, processes)
    elif run_args['script'] == 'cfexpr_searcher':
        execute_cfexpr_searcher(run_args, print_type, processes)
    else:
        raise Exception('Bad "script" parameter')

def check_processes(processes):
    while processes:
        for proc in processes:
            retcode = proc.poll()
            if retcode is not None:
                processes.remove(proc)
                break
            else:
                sleep(1)
                continue
    print('Done')

def main():
    with open(args.input, 'r') as yaml_file:
        processes = []
        for run_args in load_yaml(yaml_file.read()):
            execute(run_args, args.print, processes)
        check_processes(processes)

if __name__ == '__main__':
    main()
